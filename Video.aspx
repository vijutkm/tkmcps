﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Video.aspx.cs" Inherits="GalleryPhotos" %>
<%@ Register Src="~/header.ascx" TagPrefix="uc1" TagName="header" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>TKM Centenary Public School</title>
    <meta name="description" content="TKM Centenary Public School, Kollam, TKM CPS Kollam," />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />
    <!-- Normalize CSS -->
    <link rel="stylesheet" href="css/normalize.css" />
    <!-- Main CSS -->
    <link rel="stylesheet" href="css/main.css" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Animate CSS -->
    <link rel="stylesheet" href="css/animate.min.css" />
    <!-- Font-awesome CSS-->
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <!-- Owl Caousel CSS -->
    <link rel="stylesheet" href="vendor/OwlCarousel/owl.carousel.min.css" />
    <link rel="stylesheet" href="vendor/OwlCarousel/owl.theme.default.min.css" />
    <!-- Main Menu CSS -->
    <link rel="stylesheet" href="css/meanmenu.min.css" />
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="vendor/slider/css/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="vendor/slider/css/preview.css" type="text/css" media="screen" />
    <!-- Datetime Picker Style CSS -->
    <link rel="stylesheet" href="css/jquery.datetimepicker.css" />
    <!-- Magic popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <!-- Switch Style CSS -->
    <link rel="stylesheet" href="css/hover-min.css" />
    <!-- ReImageGrid CSS -->
    <link rel="stylesheet" href="css/reImageGrid.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="style.css" />
    <link rel="stylesheet" href="css/customstyle.css" />
    <!-- Modernizr Js -->
    <script src="js/modernizr-2.8.3.min.js"></script>
    <script>
        function show(_this) {
            document.getElementById("enlarge_images").innerHTML = "<img src='" + _this.src + "'+'width=600 height=400' >";
        }
        function hide(_this) {
            document.getElementById("enlarge_images").innerHTML = "";
        }
    </script>
    <style>
        .ImageButton {
            padding:10px;
        }
    </style>
    
     <style>
       
       
        .displaynone  {
  display:none !important;
}
    </style>
</head>
<body>
    <uc1:header runat="server" ID="header" />
    <form id="Form1" method="post" enctype="multipart/form-data" runat="server" style="width: 50%;margin: auto;">
        <asp:Label ID="Label4" style="display:none;" runat="server" Text='<%#  Session["username"].ToString() %>'></asp:Label>

        <asp:Panel ID="panelList" runat="server" GroupingText="Add Video" style="padding-left: 30px;" Font-Names="Arial">

            <div >
                <table style="margin-left:100px;margin-bottom:30px;">
                    <tr>
                        <td>Youtube Share link</td>
                        <td style="padding-left:20px;">
                            <asp:TextBox ID="txt_youtube" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                     <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                     <tr>
                        <td></td>
                        <td style="padding-left:20px;"><asp:Button ID="btnSubmit" runat="server" Text="Save" OnClick="btnSubmit_Click" Font-Names="Arial" />
                            <br /><br />
                            <asp:Label runat="server" id="lbl_message" CssClass="has-error"> </asp:Label>
                        </td>
                    </tr>
                </table>
                
            
                
            </div>

        </asp:Panel>
        <div style="padding-left:30px;">
        </div>
    </form>
</body>
</html>
