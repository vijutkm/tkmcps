﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="LogIn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>TKM Centenary Public School</title>
    <meta name="description" content="TKM Centenary Public School, Kollam, TKM CPS Kollam," />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />
    <!-- Normalize CSS -->
    <link rel="stylesheet" href="css/normalize.css" />
    <!-- Main CSS -->
    <link rel="stylesheet" href="css/main.css" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Animate CSS -->
    <link rel="stylesheet" href="css/animate.min.css" />
    <!-- Font-awesome CSS-->
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <!-- Owl Caousel CSS -->
    <link rel="stylesheet" href="vendor/OwlCarousel/owl.carousel.min.css" />
    <link rel="stylesheet" href="vendor/OwlCarousel/owl.theme.default.min.css" />
    <!-- Main Menu CSS -->
    <link rel="stylesheet" href="css/meanmenu.min.css" />
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="vendor/slider/css/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="vendor/slider/css/preview.css" type="text/css" media="screen" />
    <!-- Datetime Picker Style CSS -->
    <link rel="stylesheet" href="css/jquery.datetimepicker.css" />
    <!-- Magic popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <!-- Switch Style CSS -->
    <link rel="stylesheet" href="css/hover-min.css" />
    <!-- ReImageGrid CSS -->
    <link rel="stylesheet" href="css/reImageGrid.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="style.css" />
    <link rel="stylesheet" href="css/customstyle.css" />
    <!-- Modernizr Js -->
    <script src="js/modernizr-2.8.3.min.js"></script>
    <style>
        .dashborad-page {
            border: 1px solid #002147;
            vertical-align:middle;
            padding-top:50px;
            width: 50%;
            margin: auto;
            min-height:250px;
            background-color: #002147;
            border-top: 1px solid #ffffff;
        }
        a {
            color: #ffffff;
        }
        .homeDiv
{
    float:left;

	width: 200px;
	height: 150px;
	padding: 5px;
	color: #FFF;
	font-family: sans-serif;
	font-size: 12px;
	border-width: 3px;
	border-style: solid;
	cursor: default;
	-webkit-transition: 0.1s all;
	-moz-transition: 0.1s all;
	-ms-transition: 0.1s all;
	transition: 0.1s all;
	vertical-align:middle;
	text-align:center;
	padding-top:30px;
	margin:2px;
}
.homeDiv:hover
{
  -webkit-transform: scale(0.97) perspective(400px) rotateY(0deg) rotateX(0deg);
	-moz-transform: scale(0.97) perspective(400px) rotateY(0deg) rotateX(0deg);
	-ms-transform: scale(0.97) perspective(400px) rotateY(0deg) rotateX(0deg);
	transform: scale(0.97) perspective(400px) rotateY(0deg) rotateX(0deg);
	border-width: 3px;
	border-style:solid; 
}
.homeDiv a
{

    color:#FFFFFF;
    text-decoration:none;
    font-weight:bold;
    font-size:14px;

}
.Orange
{
    height:200px;
    background-color: #FA6800;
	border-color: #FA6800;
}
.Blue
{
        height:200px;
    background-color: #2878EC;
	border-color: #2878EC;
}
.Green
{
        height:200px;
    background-color: #60A917;
	border-color: #60A917;
    
}
.Purple
{
        height:200px;
    background-color: #800080;
	border-color: #800080;
    
}
    </style>
</head>
<body>
    <div class="wrapper">
        <header>
            <div id="header2" class="header4-area">
                <div class="header-top-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <div class="header-top-left">
                                    <div class="logo-area">
                                        <a href="index.html">
                                            <img class="img-responsive" src="img/logo-primary.png" alt="logo"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="header-top-right">
                                    <ul>
                                        <!--<li><i class="fa fa-phone" aria-hidden="true"></i><a href="Tel:+1234567890"> + 123 456 78910 </a></li>
                                        <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#">info@academics.com</a></li>-->
                                        <li>
                                            <a class="login-btn-area" href="LogIn.aspx" id=""></a>
                                           
                                        </li>
                                        <li><a href="OnlineApplication.aspx" class="apply-now-btn2">Apply Now</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-menu-area bg-primary" id="sticker">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <nav id="desktop-nav">
                                    <ul>
                                        <li class="active"><a href="index.html">Home</a></li>
                                        <li><a href="#">About us</a>
                                            <ul>
                                                <li><a href="the-institution.html">The Institution</a></li>
                                                <li><a href="infrastructure.html">Infrastructure</a></li>
                                                <li><a href="vision-mission.html">Vision & Mission</a></li>
                                                <li><a href="Doc/News_Letter_2018_2019.pdf" target="new">News Letter</a></li>
                                                <li><a href="awards.html">Awards</a></li>
                                                <li><a href="events/annualreport.pdf" target="new">Annual Report</a></li>

                                            </ul>
                                        </li>
                                        <li><a href="#">Administration</a>
                                            <ul>
                                                <li><a href="the-founder.html">The Founder</a></li>
                                                <li><a href="management.html">Management</a></li>
                                                <li><a href="principal.html">Principal</a></li>
                                                <li><a href="viceprincipal.html">Vice Principal</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="activities.html">Activities</a></li>
                                        <li><a href="#">Academics</a>
                                            <ul>
                                                <li><a href="admission.html">Admission</a></li>
                                                <li><a href="students-profile.html">Students Profile</a></li>
                                                <li><a href="rules.html">Rules</a></li>
                                                <li><a href="events/results.pdf" target="new">Results</a></li>
                                            </ul>
                                        </li>

                                        <li><a href="gallery.aspx">Gallery</a></li>
                                        <li><a href="contactus.html">Contact us</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu Area Start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                        <li class="active"><a href="index.html">Home</a></li>
                                        <li><a href="#">About us</a>
                                            <ul>
                                                <li><a href="the-institution.html">The Institution</a></li>
                                                <li><a href="infrastructure.html">Infrastructure</a></li>
                                                <li><a href="vision-mission.html">Vision & Mission</a></li>
                                                <li><a href="Doc/News_Letter_2018_2019.pdf" target="new">News Letter</a></li>
                                                <li><a href="awards.html">Awards</a></li>
                                                <li><a href="events/annualreport.pdf" target="new">Annual Report</a></li>

                                            </ul>
                                        </li>
                                        <li><a href="#">Administration</a>
                                            <ul>
                                                <li><a href="the-founder.html">The Founder</a></li>
                                                <li><a href="management.html">Management</a></li>
                                                <li><a href="principal.html">Principal</a></li>
                                                <li><a href="viceprincipal.html">Vice Principal</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="activities.html">Activities</a></li>
                                        <li><a href="#">Academics</a>
                                            <ul>
                                                <li><a href="admission.html">Admission</a></li>
                                                <li><a href="staffprofile.html" >Staffs Profile</a></li>
                                                <li><a href="students-profile.html">Students Profile</a></li>
                                                <li><a href="rules.html">Rules</a></li>
                                                <li><a href="events/results.pdf" target="new">Results</a></li>
                                            </ul>
                                        </li>

                                        <li><a href="gallery.aspx">Gallery</a></li>
                                        <li><a href="contactus.html">Contact us</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu Area End -->
        </header>
        <div >
            
                
            <br />
                <div class="title-default-left-bold" style="text-align:center">Login</div>
                                            <form runat="server" class="login-form" style="margin:auto ;position:static">
                                                <label>Username*</label>
                                                <asp:TextBox runat="server" ID="TxtUsername" placeholder="Username" name="uname" Font-Names="Arial"></asp:TextBox>
                                                <label>Password *</label>
                                                <asp:TextBox runat="server" ID="TxtPassword" placeholder="Password" Font-Names="Arial" TextMode="Password"></asp:TextBox>
                                                <asp:Label ID="LblMessage" runat="server" Font-Names="Arial" ForeColor="Red"></asp:Label>
                                                <asp:Button runat="server" Text="Login"  ID="btnLogin" OnClick="btnLogin_Click" />

                                            </form>
            

                  
            
        </div>
    </div>
</body>
</html>




