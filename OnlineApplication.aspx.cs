﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class OnlineApplication : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        //var fromAddress = "tkmtrust@tkminfotech.com";
        var fromAddress = "tkmcps1@gmail.com";
        var toAddress = "kvijur@gmail.com";// "tkmcpsadmn16@gmail.com";
        const string fromPassword = "asd@123*";
        string subject = "Appication_" + txtFullName.Value;
        string body = "<B> Following is the Application Submitted by "+ txtFullName.Value+"</B><br> ";
         body += "<br><br><html><body><table border='1'> <tr><td>Full Name</td><td> "+txtFullName.Value+"</td></tr>"+
                  "<tr><td>Class</td><td> " + txtClass.Value + "</td></tr>" +
                  "<tr><td>DOB</td><td> " + datepicker.Value + "</td></tr>" +
                  "<tr><td>Gender</td><td> " + ddlGender.Value + "</td></tr>" +
                  "<tr><td>Father Name</td><td> " + txtFather.Value + "</td></tr>" +
                  "<tr><td>Parent Occupation</td><td> " + txtOccupation.Value + "</td></tr>" +
                  "<tr><td>Contact</td><td> " + txtContact.Value + "</td></tr>" +
                  "<tr><td>Email Id</td><td> " + txtEmail.Value + "</td></tr>" +
                  "<tr><td>Previous School</td><td> " + txtSchoolName.Value + "</td></tr>" +
                  "<tr><td>Parent Address</td><td> " + txtAddress.Value + "</td></tr></table></body></html>";

        MailMessage mailMessage = new MailMessage();
        mailMessage.From = new MailAddress(fromAddress);
        mailMessage.Subject = subject;
        mailMessage.IsBodyHtml= true;
        mailMessage.Body = body;
      
        mailMessage.To.Add(new MailAddress(toAddress));
        // smtp settings
        var smtp = new System.Net.Mail.SmtpClient();
        {
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 25;
            smtp.EnableSsl = true;
            //smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            smtp.Credentials = new System.Net.NetworkCredential(fromAddress, fromPassword);
            //smtp.Timeout = 20000;
        }
        //Passing values to smtp object
        smtp.Send(mailMessage);
        txtFullName.Value = "";
        txtClass.Value = "";
        datepicker.Value = "";
        ddlGender.Value = "Gender";
        txtFather.Value = "";
        txtOccupation.Value = "";
        txtContact.Value = "";
        txtEmail.Value = "";
        txtAddress.Value = "";
        txtSchoolName.Value = "";

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertMessage", "alert('Application Submitted Successfully');", true);

    
    }
}