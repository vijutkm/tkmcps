﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LogIn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string username = ConfigurationManager.AppSettings["Username"];
        string password = ConfigurationManager.AppSettings["Password"];
        Session["username"] = Convert.ToString(username);
        if (TxtUsername.Text == username && TxtPassword.Text == password)
        {
            Response.Redirect("Dashboard.aspx");
        }
        else
        {
            LblMessage.Text = "Invalid Username or Password.";
        }
    }
}