﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class events : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            Label4.Text = Convert.ToString(Session["username"]);
            //BindGrid();
        }

    }




    public void BindGrid()
    {




        string dirPath = Server.MapPath("~/eventgallery/");

        List<string> dirs = new List<string>(Directory.EnumerateDirectories(dirPath));
        List<ListItem> Imgs = new List<ListItem>();


        string dirPat = @"~/eventgallery";

        var jpgs = System.IO.Directory.GetFiles(Server.MapPath(dirPat), "*g*.jpg").Select(f => System.IO.Path.GetFileNameWithoutExtension(f)).ToList().Select(x => x.Replace("g", string.Empty));

        var jpgs2 = jpgs.Where(x => !x.Contains("deleted_"));


        var jpgs1 = jpgs2.OrderBy(o => int.Parse(o.ToString())).ToList();

        string[] ImagePaths2 = jpgs.Select(f => f + ".jpg")
                       .ToArray();



        foreach (string imgPath in jpgs1)
        {
            string ImgName = Path.GetFileName(imgPath);
            Imgs.Add(new ListItem(ImgName, "eventgallery/g" + ImgName + ".jpg"));
        }


      

    }


    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string item = e.Row.Cells[0].Text;
            foreach (Button button in e.Row.Cells[2].Controls.OfType<Button>())
            {
                if (button.CommandName == "Delete")
                {
                    button.Attributes["onclick"] = "if(!confirm('Do you want to delete " + item + "?')){ return false; };";
                }
            }
        }
    }

    protected void dlImages_DeleteCommand(object source, DataListCommandEventArgs e)
    {

      


    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
        {



            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            string[] splitted = fn.Split('.');
            string SaveLocation = Server.MapPath("events") + "\\" + ddl_category.SelectedValue+"." + splitted[splitted.Length-1];
            try
            {
                FileUpload1.PostedFile.SaveAs(SaveLocation);
                Response.Write("The file has been uploaded.");
                //BindGrid();
                ddl_category.Items[0].Selected = true;
            }
            catch (Exception ex)
            {
                Response.Write("Error Occured while uploading photo. ");
            }
        }
        else
        {
            Response.Write("Please select a file to upload.");
        }
    }
}