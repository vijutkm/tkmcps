﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Dashboard" %>

<%@ Register Src="~/header.ascx" TagPrefix="uc1" TagName="header" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>TKM Centenary Public School</title>
    <meta name="description" content="TKM Centenary Public School, Kollam, TKM CPS Kollam," />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />
    <!-- Normalize CSS -->
    <link rel="stylesheet" href="css/normalize.css" />
    <!-- Main CSS -->
    <link rel="stylesheet" href="css/main.css" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Animate CSS -->
    <link rel="stylesheet" href="css/animate.min.css" />
    <!-- Font-awesome CSS-->
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <!-- Owl Caousel CSS -->
    <link rel="stylesheet" href="vendor/OwlCarousel/owl.carousel.min.css" />
    <link rel="stylesheet" href="vendor/OwlCarousel/owl.theme.default.min.css" />
    <!-- Main Menu CSS -->
    <link rel="stylesheet" href="css/meanmenu.min.css" />
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="vendor/slider/css/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="vendor/slider/css/preview.css" type="text/css" media="screen" />
    <!-- Datetime Picker Style CSS -->
    <link rel="stylesheet" href="css/jquery.datetimepicker.css" />
    <!-- Magic popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <!-- Switch Style CSS -->
    <link rel="stylesheet" href="css/hover-min.css" />
    <!-- ReImageGrid CSS -->
    <link rel="stylesheet" href="css/reImageGrid.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="style.css" />
    <link rel="stylesheet" href="css/customstyle.css" />
    <!-- Modernizr Js -->
    <script src="js/modernizr-2.8.3.min.js"></script>
    <style>
        .dashborad-page {
            border: 1px solid #002147;
            vertical-align:middle;
            padding-top:50px;
            width: 50%;
            margin: auto;
            min-height:250px;
            background-color: #002147;
            border-top: 1px solid #ffffff;
        }
        a {
            color: #ffffff;
        }
        .homeDiv
{
    float:left;

	width: 200px;
	height: 150px;
	padding: 5px;
	color: #FFF;
	font-family: sans-serif;
	font-size: 12px;
	border-width: 3px;
	border-style: solid;
	cursor: default;
	-webkit-transition: 0.1s all;
	-moz-transition: 0.1s all;
	-ms-transition: 0.1s all;
	transition: 0.1s all;
	vertical-align:middle;
	text-align:center;
	padding-top:30px;
	margin:2px;
}
.homeDiv:hover
{
  -webkit-transform: scale(0.97) perspective(400px) rotateY(0deg) rotateX(0deg);
	-moz-transform: scale(0.97) perspective(400px) rotateY(0deg) rotateX(0deg);
	-ms-transform: scale(0.97) perspective(400px) rotateY(0deg) rotateX(0deg);
	transform: scale(0.97) perspective(400px) rotateY(0deg) rotateX(0deg);
	border-width: 3px;
	border-style:solid; 
}
.homeDiv a
{

    color:#FFFFFF;
    text-decoration:none;
    font-weight:bold;
    font-size:14px;

}
.Orange
{
    height:200px;
    background-color: #FA6800;
	border-color: #FA6800;
}
.Blue
{
        height:200px;
    background-color: #2878EC;
	border-color: #2878EC;
}
.Green
{
        height:200px;
    background-color: #60A917;
	border-color: #60A917;
    
}
.Purple
{
        height:200px;
    background-color: #800080;
	border-color: #800080;
    
}
    </style>
</head>
<body>
    <div class="wrapper">
        <uc1:header runat="server" ID="header" />
        <div style="margin:auto;padding:10%;" >
            
                

                 <div class="homeDiv Orange">
                 <a href="GalleryPhotos.aspx">
                 <img alt="Edit Profile" src="img/gallery.png" width="100px" height="100px" /><br />
                 Gallery Photos</a>
                 </div>

                 <div class="homeDiv Blue">
                 <a href="events.aspx">
                 <img alt="Edit Profile" src="img/events.png" width="100px" height="100px" /><br />
                Events</a>
                 </div>

                     <div class="homeDiv Green">
                 <a href="News.aspx">
                 <img alt="Edit Profile" src="img/News.png" width="100px" height="100px" /><br />
                 News</a>
                 </div>

                  <div class="homeDiv Purple">
                 <a href="video.aspx">
                 <img alt="Videos" src="img/videos.png" width="100px" height="100px" /><br />
                 Videos</a>
                 </div>

                  
            
        </div>
    </div>
</body>
</html>
