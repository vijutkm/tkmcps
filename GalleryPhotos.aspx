﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GalleryPhotos.aspx.cs" Inherits="GalleryPhotos" %>
<%@ Register Src="~/header.ascx" TagPrefix="uc1" TagName="header" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>TKM Centenary Public School</title>
    <meta name="description" content="TKM Centenary Public School, Kollam, TKM CPS Kollam," />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />
    <!-- Normalize CSS -->
    <link rel="stylesheet" href="css/normalize.css" />
    <!-- Main CSS -->
    <link rel="stylesheet" href="css/main.css" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Animate CSS -->
    <link rel="stylesheet" href="css/animate.min.css" />
    <!-- Font-awesome CSS-->
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <!-- Owl Caousel CSS -->
    <link rel="stylesheet" href="vendor/OwlCarousel/owl.carousel.min.css" />
    <link rel="stylesheet" href="vendor/OwlCarousel/owl.theme.default.min.css" />
    <!-- Main Menu CSS -->
    <link rel="stylesheet" href="css/meanmenu.min.css" />
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="vendor/slider/css/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="vendor/slider/css/preview.css" type="text/css" media="screen" />
    <!-- Datetime Picker Style CSS -->
    <link rel="stylesheet" href="css/jquery.datetimepicker.css" />
    <!-- Magic popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <!-- Switch Style CSS -->
    <link rel="stylesheet" href="css/hover-min.css" />
    <!-- ReImageGrid CSS -->
    <link rel="stylesheet" href="css/reImageGrid.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="style.css" />
    <link rel="stylesheet" href="css/customstyle.css" />
    <!-- Modernizr Js -->
    <script src="js/modernizr-2.8.3.min.js"></script>
    <script>
        function show(_this) {
            document.getElementById("enlarge_images").innerHTML = "<img src='" + _this.src + "'+'width=600 height=400' >";
        }
        function hide(_this) {
            document.getElementById("enlarge_images").innerHTML = "";
        }
    </script>
    <style>
        .ImageButton {
            padding:10px;
        }
    </style>
    
     <style>
       
       
        .displaynone  {
  display:none !important;
}
    </style>
</head>
<body>
    <uc1:header runat="server" ID="header" />
    <form id="Form1" method="post" enctype="multipart/form-data" runat="server" style="width: 50%;margin: auto;">
        <asp:Label ID="Label4" style="display:none;" runat="server" Text='<%#  Session["username"].ToString() %>'></asp:Label>

        <asp:Panel ID="panelList" runat="server" GroupingText="Add Photo" style="padding-left: 30px;" Font-Names="Arial">

            <div >
                <table style="margin-left:100px;margin-bottom:30px;">
                    <tr>
                        <td>Category</td>
                        <td style="padding-left:20px;">
                            <asp:DropDownList ID="ddl_category" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_category_SelectedIndexChanged">
                                <asp:ListItem Text="Library" Value="Library"></asp:ListItem>
                                <asp:ListItem Text="Class Room" Value="ClassRoom"></asp:ListItem>
                                <asp:ListItem Text="Auditoriam" Value="Auditorium"></asp:ListItem>
                                <asp:ListItem Text="Campus" Value="Campus"></asp:ListItem>
                                <asp:ListItem Text="Activities" Value="Activities"></asp:ListItem>
                               
                            </asp:DropDownList></td>
                    </tr>
                     <tr>
                        <td>Photo</td>
                        <td><asp:FileUpload ID="FileUpload1" runat="server" Style="padding: 20px;" /></td>
                    </tr>
                     <tr>
                        <td></td>
                        <td style="padding-left:20px;"><asp:Button ID="btnSubmit" runat="server" Text="Upload Photo" OnClick="btnSubmit_Click" Font-Names="Arial" /></td>
                    </tr>
                </table>
                
            
                
            </div>

        </asp:Panel>
        <div style="padding-left:30px;">
            <asp:Panel ID="panel1" runat="server" GroupingText="Photo List" Font-Names="Arial">
                <div style="float: left; padding: 10px; border: 1px solid black; height: 600px; overflow-y: scroll;width: 93%;">

                    <asp:DataList  ID="ds_image" runat="server" RepeatColumns="4" 
                                    RepeatDirection="Horizontal" CellPadding="50" 
                                    CellSpacing="50"
                                    UseAccessibleHeader="True" OnDataBinding="ds_image_DataBinding" OnItemDataBound="ds_image_ItemDataBound" DataKeyField="Id" OnDeleteCommand="ds_image_DeleteCommand" >
                                    <ItemTemplate>
                                    <asp:ImageButton CssClass="ImageButton" Width="120" Height="120" ID="ImageData" ImageUrl='<%# Eval("filename", "") %>' runat="server" /><br />
                                        <asp:LinkButton id="DeleteButton" Text=" Delete Image" CommandName="Delete" runat="server"/>
                                     </ItemTemplate>

                                   
                    </asp:DataList>



                </div>
                <div id="enlarge_images" style="position: absolute; z-index: 2; display: inline-block; float: right; border: 1px solid black;"></div>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
