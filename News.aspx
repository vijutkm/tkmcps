﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="News.aspx.cs" Inherits="News" %>
<%@ Register Src="~/header.ascx" TagPrefix="uc1" TagName="header" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>TKM Centenary Public School</title>
    <meta name="description" content="TKM Centenary Public School, Kollam, TKM CPS Kollam," />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />
    <!-- Normalize CSS -->
    <link rel="stylesheet" href="css/normalize.css" />
    <!-- Main CSS -->
    <link rel="stylesheet" href="css/main.css" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Animate CSS -->
    <link rel="stylesheet" href="css/animate.min.css" />
    <!-- Font-awesome CSS-->
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <!-- Owl Caousel CSS -->
    <link rel="stylesheet" href="vendor/OwlCarousel/owl.carousel.min.css" />
    <link rel="stylesheet" href="vendor/OwlCarousel/owl.theme.default.min.css" />
    <!-- Main Menu CSS -->
    <link rel="stylesheet" href="css/meanmenu.min.css" />
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="vendor/slider/css/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="vendor/slider/css/preview.css" type="text/css" media="screen" />
    <!-- Datetime Picker Style CSS -->
    <link rel="stylesheet" href="css/jquery.datetimepicker.css" />
    <!-- Magic popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <!-- Switch Style CSS -->
    <link rel="stylesheet" href="css/hover-min.css" />
    <!-- ReImageGrid CSS -->
    <link rel="stylesheet" href="css/reImageGrid.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="style.css" />
    <link rel="stylesheet" href="css/customstyle.css" />
    <!-- Modernizr Js -->
    <script src="js/modernizr-2.8.3.min.js"></script>
    <style>
        .dashborad-page {
            border: 1px solid #002147;
            vertical-align:middle;
            padding-top:50px;
            width: 50%;
            margin: auto;
            min-height:250px;
            background-color: #002147;
            border-top: 1px solid #ffffff;
        }
        a {
            color: #ffffff;
        }
        .homeDiv
{
    float:left;

	width: 200px;
	height: 150px;
	padding: 5px;
	color: #FFF;
	font-family: sans-serif;
	font-size: 12px;
	border-width: 3px;
	border-style: solid;
	cursor: default;
	-webkit-transition: 0.1s all;
	-moz-transition: 0.1s all;
	-ms-transition: 0.1s all;
	transition: 0.1s all;
	vertical-align:middle;
	text-align:center;
	padding-top:30px;
	margin:2px;
}
.homeDiv:hover
{
  -webkit-transform: scale(0.97) perspective(400px) rotateY(0deg) rotateX(0deg);
	-moz-transform: scale(0.97) perspective(400px) rotateY(0deg) rotateX(0deg);
	-ms-transform: scale(0.97) perspective(400px) rotateY(0deg) rotateX(0deg);
	transform: scale(0.97) perspective(400px) rotateY(0deg) rotateX(0deg);
	border-width: 3px;
	border-style:solid; 
}
.homeDiv a
{

    color:#FFFFFF;
    text-decoration:none;
    font-weight:bold;
    font-size:14px;

}
.Orange
{
    height:200px;
    background-color: #FA6800;
	border-color: #FA6800;
}
.Blue
{
        height:200px;
    background-color: #2878EC;
	border-color: #2878EC;
}
.Green
{
        height:200px;
    background-color: #60A917;
	border-color: #60A917;
    
}
.Purple
{
        height:200px;
    background-color: #800080;
	border-color: #800080;
    
}

.gridview {
        font-family:"arial";
        background-color:#FFFFFF;
        width: 100%;
        font-size: small;
}
.gridview th {
       background-color: rgba(0, 33, 71, 0.8);
        padding: 5px;
        font-size:small;
         color: #FFFFFF;
         text-align:left;

}
.gridview th a{
        color: #003300;
        text-decoration: none;
}
.gridview th a:hover{
        color: #003300;
        text-decoration: underline;
}
.gridview td  {
        background: #D9EDC9;
        color: #333333;
        font: small "arial";
        padding: 4px;
}
.gridview tr.even td {
        background: #FFFFFF;
}
.gridview td a{
        color: #003300;
        font: bold small "arial";
        padding: 2px;
        text-decoration: none;
}
.gridview td a:hover {
        color: red;
        font-weight: bold;
        text-decoration:underline;     
} 
.bg {
    order-radius: 0;
    background: #f1f1f1;
    box-shadow: none;
    border: none;
}
    </style>
</head>
<body>
    <div class="wrapper">
            <uc1:header runat="server" ID="header" />
        <div style="margin:auto;padding:7%;" >
            <form id="form1" runat="server" enctype="multipart/form-data">
                                            <table style=" height: 440px;">
                                                <tr>
                                                   <td class="auto-style2">
                                                        <asp:Panel ID="panelList" runat="server" GroupingText="News List" Font-Names="Arial">
                                                        <div class="GridviewDiv">
                                                         <%-- OnPageIndexChanging="gvDetails_PageIndexChanging" 
                                           OnRowCommand ="gvDetails_RowCommand"--%>
                                                        <asp:GridView ID="gvDetails" runat="server"  AutoGenerateColumns="False" Height="299px" OnRowUpdating="gvDetails_RowUpdating"
                                                          OnRowEditing="gvDetails_RowEditing" OnRowCancelingEdit="gvDetails_RowCancelingEdit" DataKeyNames="ID" ShowFooter="True" AllowPaging="True"
                                                           OnRowDeleting="gvDetails_RowDeleting" OnRowDataBound="GridView1_RowDataBound"
                                                             CssClass="gridview"  AlternatingRowStyle-CssClass="even" CellPadding="4" ForeColor="#333333" GridLines="Horizontal">

                                                            <AlternatingRowStyle BackColor="White" CssClass="even" ForeColor="#284775" />
                                                            <Columns>
                                                              <%--  <asp:BoundField DataField="id" HeaderText="Id" ReadOnly="true" />--%>
                                                                <asp:TemplateField HeaderText="Heading">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblHeading" runat="server" Text='<%# Eval("Heading")%>' />
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtHeading" runat="server" Text='<%# Eval("Heading")%>' />
                                                                        <%-- <asp:RequiredFieldValidator ValidationGroup="Update" ID="reqheader1" runat="server" ControlToValidate="txtHeading" Text="Please enter Header" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                                                    </EditItemTemplate>
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Sub Heading">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblsub_heading" runat="server" Text='<%# Eval("News")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txt_sub_heading" runat="server" Text='<%# Eval("News")%>' />
                                                                             </EditItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="News">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblNews" runat="server" Width="300px"  Text='<%# Eval("Details")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtNewsitem" runat="server" Text='<%# Eval("Details")%>' />
                                                                        <asp:RequiredFieldValidator ID="reqheader2" Width="300px" runat="server" ControlToValidate="txtNewsitem" ForeColor="Red" Text="Please enter News" ValidationGroup="Update"></asp:RequiredFieldValidator>
                                                                    </EditItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="File">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="ImageData" runat="server" CssClass="ImageButton" Height="120" ImageUrl='<%# Eval("Filepath") %>' Width="120" />
                                                                        <asp:Label ID="lblFilepath" runat="server" CssClass="wrap" Text='<%# Eval("Filepath")%>' Visible="false" Width="200px"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                                                    </EditItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ShowHeader="False">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="DeleteButton" runat="server" AlternateText="Delete" CommandName="Delete" Height="20px" ImageUrl="~/img/Delete_Icon.png" OnClientClick="return confirm('Are you sure you want to delete this item?');" ToolTip="Delete" Width="24px" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:CommandField ShowEditButton="True" ValidationGroup="Update" />
                                                            </Columns>
                                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    
                                                           <RowStyle Height="35px" Width="200px" Wrap="true" BackColor="#F7F6F3" ForeColor="#333333" />
                                                            <EditRowStyle BackColor="#999999" />
                                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                                            <HeaderStyle CssClass="headerstyle" BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                                        </asp:GridView>
                                        </div>

                                                            </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Panel ID="panel1" runat="server" GroupingText="Add News" Font-Names="Arial" style="font-family:Arial;padding-top: 50px;">
                                                        <table>
                                                             <tr>
                                                               <td class="auto-style1">     
                                                               <asp:TextBox ID="txtheader"  placeholder=" Heading*" class="bg" runat="server" Height="30px" Width="500px" Font-Names="Arial"></asp:TextBox>
                                                             
                   <asp:RequiredFieldValidator ValidationGroup="Add" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtheader" Text="Please enter News Heading" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </td>
                                                            <td class="auto-style2">

                                                                <asp:TextBox ID="txt_sub_header"  placeholder=" Sub Heading" class="bg" runat="server" Height="30px" Width="500px" Font-Names="Arial"></asp:TextBox>
                                                              <%--  <asp:RequiredFieldValidator ValidationGroup="Add" ID="reqheader" runat="server" ControlToValidate="txtheader" Text="Please enter Header" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                                            </td>
                                                        </tr>
                                                            <tr>
                                                                <td>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                        <tr>
                                                            <td class="auto-style1">                
                                                                      <asp:TextBox placeholder=" News*" ID="txtnews" class="bg" runat="server" Height="75px" Width="500px" TextMode="MultiLine" Font-Names="Arial"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ValidationGroup="Add" ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtnews" Text="Please enter News" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                  
                
                                                                </td>
                                                                  <td class="auto-style2">

                                                               </td>
                                                            </tr>
  
            
                                                 <tr>
                                                    <td class="auto-style1">
                
                                                        <asp:Label ID="Label3" runat="server" Text="Upload File" Font-Names="Arial"></asp:Label>
                
                                                    </td>
                                                    <td class="auto-style2">

                                                        <asp:FileUpload ID="pdfupload" runat="server" Width="264px" Font-Names="Arial" />

                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td colspan="2" class="auto-style3" style="text-align:center;">
               

                                                        <asp:Button ID="btnSave" runat="server" class="default-big-btn disabled" ValidationGroup="Add" Text="Save" Width="85px" OnClick="btnSave_Click" />
               

                                                    </td>
                                                </tr>
                                                            </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                            </table>
                                            </form>
                



                  
            
        </div>
    </div>
</body>
</html>
