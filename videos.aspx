﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="videos.aspx.cs" Inherits="videos" %>
<%@ Register Src="~/header.ascx" TagPrefix="uc1" TagName="header" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <title>TKM Centenary Public School</title>
    <meta name="description" content="TKM Centenary Public School, Kollam, TKM CPS Kollam," />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />
    <!-- Normalize CSS -->
    <link rel="stylesheet" href="css/normalize.css" />
    <!-- Main CSS -->
    <link rel="stylesheet" href="css/main.css" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Animate CSS -->
    <link rel="stylesheet" href="css/animate.min.css" />
    <!-- Font-awesome CSS-->
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <!-- Owl Caousel CSS -->
    <link rel="stylesheet" href="vendor/OwlCarousel/owl.carousel.min.css" />
    <link rel="stylesheet" href="vendor/OwlCarousel/owl.theme.default.min.css" />
    <!-- Main Menu CSS -->
    <link rel="stylesheet" href="css/meanmenu.min.css" />
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="vendor/slider/css/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="vendor/slider/css/preview.css" type="text/css" media="screen" />
    <!-- Datetime Picker Style CSS -->
    <link rel="stylesheet" href="css/jquery.datetimepicker.css" />
    <!-- Magic popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <!-- Switch Style CSS -->
    <link rel="stylesheet" href="css/hover-min.css" />
    <!-- ReImageGrid CSS -->
    <link rel="stylesheet" href="css/reImageGrid.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="style.css" />
    <link rel="stylesheet" href="css/customstyle.css" />
    <!-- Modernizr Js -->
    <script src="js/modernizr-2.8.3.min.js"></script>
    <script>
        function show(_this) {
            document.getElementById("enlarge_images").innerHTML = "<img src='" + _this.src + "'+'width=600 height=400' >";
        }
        function hide(_this) {
            document.getElementById("enlarge_images").innerHTML = "";
        }
    </script>
</head>
<body>
        <uc1:header runat="server" ID="header" />
    <form id="Form1" method="post" enctype="multipart/form-data" runat="server" style="width: 41%; margin: auto;">
        <asp:Label ID="Label4" Style="display: none;" runat="server" Text='<%#  Session["username"].ToString() %>'></asp:Label>

        <asp:Panel ID="panelList" runat="server" GroupingText="Add Video" Style="padding-left: 30px;" Font-Names="Arial">

            <div style="float: left; padding-left: 200px; border: 1px solid black;">
                <asp:FileUpload ID="FileUpload1" runat="server" Style="padding-bottom: 20px;" />
                <br>
                <asp:Button ID="btnSubmit" runat="server" Text="Upload Video" OnClick="btnSubmit_Click" Font-Names="Arial" />
            </div>

        </asp:Panel>
        <div style="padding-left: 30px;">
            <asp:Panel ID="panel1" runat="server" GroupingText="Videos" Font-Names="Arial">
                <div style="float: left; padding: 10px; border: 1px solid black; height: 600px; overflow-y: scroll; width: 93%;">
                    <asp:DataList ID="dlImages" runat="server"
                        RepeatColumns="5" DataKeyField="Value"
                        RepeatDirection="Horizontal"
                        RepeatLayout="Flow" OnDeleteCommand="dlImages_DeleteCommand">
                        <ItemTemplate>
                            <div style="border: 1px solid black; display: inline-block;">
                                <img src='<%# DataBinder.Eval(Container.DataItem, "Value") %>'
                                    style="height: 50px; width: 50px; border: 1px solid gray;" onmouseover="show(this)" onmouseout="hide(this);" />
                                <asp:Button ID="Delete" runat="server"
                                    Text="Delete"
                                    CommandName="delete" OnClientClick="return confirm('Are you sure to Remove this Image?');" />
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
                <div id="enlarge_images" style="position: absolute; z-index: 2; display: inline-block; float: right; border: 1px solid black;"></div>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
