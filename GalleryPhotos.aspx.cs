﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GalleryPhotos : System.Web.UI.Page
{
    SqlConnection con;
    SqlCommand cmd;
    string constring = ConfigurationManager.AppSettings["DbConnection"].ToString();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] == null)
            Response.Redirect("index.html");

        if (!IsPostBack)
        {
            BindImages();
        }

    }


    public void BindImages()
    {
        try
        {
            DataSet ds = new DataSet();
            using (con = new SqlConnection(constring))
            {
                cmd = new SqlCommand();
                cmd.CommandText = "select * from Photos where category='" + ddl_category.SelectedValue + "'";
                cmd.Connection = con;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(ds);
                con.Close();
            }
            ds_image.DataSource = ds;
            ds_image.DataBind();

        }
        catch (Exception ex)
        {
        }

    }

    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    protected void dlImages_DeleteCommand(object source, DataListCommandEventArgs e)
    {




    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (FileUpload1.PostedFile.FileName != "")
            {
                string strFileName = DateTime.Now.ToString("MM-dd-yyyy_HHmmss").Replace("-", "_");
                string strFileType = System.IO.Path.GetExtension(FileUpload1.FileName).ToString().ToLower();
                FileUpload1.SaveAs(Server.MapPath("gallery/" + ddl_category.SelectedValue + "/" + strFileName + strFileType));


                using (con = new SqlConnection(constring))
                {
                    cmd = new SqlCommand();
                    cmd.CommandText = "insert into Photos(filename,category) values('" + strFileName + strFileType + "','" + ddl_category.SelectedValue + "')";
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();


                }
                BindImages();
            }
        }
        catch (Exception ex)
        {
        }

    }
    protected void ds_image_DataBinding(object sender, EventArgs e)
    {


    }
    protected void ds_image_ItemDataBound(object sender, DataListItemEventArgs e)
    {


        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView dr = (DataRowView)e.Item.DataItem;
                Image ImageData = (Image)e.Item.FindControl("ImageData");
                if (dr.Row[0].ToString() != "NA")
                {
                    ImageData.Visible = true;
                    ImageData.ImageUrl = @"gallery/" + ddl_category.SelectedValue + "/" + dr.Row["filename"].ToString();
                }
                else
                    ImageData.Visible = false;


            }
        }
        catch (Exception)
        { }
    }
    protected void ddl_category_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindImages();
    }



    protected void ds_image_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        int StId = (int)ds_image.DataKeys[(int)e.Item.ItemIndex];

        try
        {
            


            using (con = new SqlConnection(constring))
            {
                cmd = new SqlCommand();
                cmd.CommandText = "delete from Photos where Id="+ StId;
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();


            }
            BindImages();
        }
        catch (Exception ex)
        {
        }
    }
}