﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class News : System.Web.UI.Page
{
    SqlConnection con;
    SqlCommand cmd;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["username"] == null)
                Response.Redirect("index.html");
            BindGridview();
        }
    }
    string constring = ConfigurationManager.AppSettings["DbConnection"].ToString();

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            string SaveLocation = "", strFileName = "", strFileType = "";
            if ((pdfupload.PostedFile != null) && (pdfupload.PostedFile.ContentLength > 0))
            {
                string fn = System.IO.Path.GetFileName(pdfupload.PostedFile.FileName);
                SaveLocation = Server.MapPath("UploadedFiles") + "\\" + fn;
                strFileName = DateTime.Now.ToString("MM-dd-yyyy_HHmmss").Replace("-", "_");
                strFileType = System.IO.Path.GetExtension(pdfupload.FileName).ToString().ToLower();
                pdfupload.SaveAs(Server.MapPath("gallery/News/" + strFileName + strFileType));
                // pdfupload.PostedFile.SaveAs(SaveLocation);

            }

            using (con = new SqlConnection(constring))
            {
                cmd = new SqlCommand();
                cmd.CommandText = "insert into newsitem(heading,details,filepath,news) values('" + txtheader.Text + "','" + txtnews.Text + "','" + strFileName + strFileType + "','" + txt_sub_header.Text + "')";
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                txtheader.Text = "";
                txtnews.Text = "";

            }
        }
        catch (Exception ex)
        {
            Response.Write("Error saving news. ");
        }
        BindGridview();
    }
    public void BindGridview()
    {
        SqlConnection con = new SqlConnection(constring);
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from NewsItem";
        cmd.Connection = con;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        try
        {
            con.Open();
            gvDetails.DataSource = ds;
            gvDetails.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            con.Close();
            con.Dispose();
        }
    }

    protected void gvDetails_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvDetails.EditIndex = e.NewEditIndex;
        BindGridview();
    }
    protected void gvDetails_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvDetails.EditIndex = -1;
        BindGridview();
    }
    protected void gvDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDetails.PageIndex = e.NewPageIndex;
        BindGridview();
    }
    protected void gvDetails_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
       
        try
        {
            int id = Convert.ToInt32(gvDetails.DataKeys[e.RowIndex].Values[0].ToString());
            TextBox txtheading = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtheading");
            TextBox txtnews = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtnewsitem");
            TextBox txt_sub_heading = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txt_sub_heading");
            FileUpload fileUpload = gvDetails.Rows[e.RowIndex].FindControl("FileUpload1") as FileUpload;

            if (txtheading.Text != "" && txtnews.Text != "")
            {
                string SaveLocation = "", strFileName = "", strFileType = "";
                if ((fileUpload.PostedFile != null) && (fileUpload.PostedFile.ContentLength > 0))
                {
                    string fn = System.IO.Path.GetFileName(fileUpload.PostedFile.FileName);
                    SaveLocation = Server.MapPath("UploadedFiles") + "\\" + fn;
                    strFileName = DateTime.Now.ToString("MM-dd-yyyy_HHmmss").Replace("-", "_");
                    strFileType = System.IO.Path.GetExtension(fileUpload.FileName).ToString().ToLower();
                    fileUpload.SaveAs(Server.MapPath("gallery/News/" + strFileName + strFileType));
                    // pdfupload.PostedFile.SaveAs(SaveLocation);

                }

                UpdateNewsItem(txtheading.Text, txt_sub_heading.Text, id, strFileName + strFileType, txtnews.Text);
                gvDetails.EditIndex = -1;
                BindGridview();
            }
        }
        catch (Exception ex)
        {
            Response.Write("Error Updating news. ");
        }

    }

    private void UpdateNewsItem(string heading, string news, Int32 id, string filepath,string Details)
    {
        using (con = new SqlConnection(constring))
        {
            cmd = new SqlCommand();
            if (filepath != "")
            {
                cmd.CommandText = "Update newsitem set heading='" + heading + "',news='" + news + "',filepath='" + filepath + "', details= '" + Details + "' where id=" + id;
            }
            else
            {
                cmd.CommandText = "Update newsitem set heading='" + heading + "',news='" + news + "', details= '" + Details + "' where id=" + id;

            }

            cmd.Connection = con;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            txtheader.Text = "";
            txtnews.Text = "";

        }
    }
    protected void gvDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Convert.ToInt32(gvDetails.DataKeys[e.RowIndex].Values[0].ToString());
        DeleteNewsItem(id);
        gvDetails.EditIndex = -1;
        BindGridview();
    }
    private void DeleteNewsItem(int id)
    {
        using (con = new SqlConnection(constring))
        {
            cmd = new SqlCommand();
            cmd.CommandText = "delete from newsitem where id=" + id;
            cmd.Connection = con;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            txtheader.Text = "";
            txtnews.Text = "";

        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Image img_down_arrow = (Image)e.Row.FindControl("ImageData");
                string url = img_down_arrow.ImageUrl;
                if (url.Contains(".pdf"))
                    url = "pdf.png";
                img_down_arrow.ImageUrl = "~/Gallery/News/" + url;
            }
        }
        catch (Exception)
        { }
    }
    [WebMethod(EnableSession = true)]
    public static string get_latest_news()
    {
        string constring = ConfigurationManager.AppSettings["DbConnection"].ToString();
        SqlConnection con = new SqlConnection(constring);
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select top 15 * from NewsItem order by Id desc";
        cmd.Connection = con;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
       
        DataTable dt = ds.Tables[0];
        System.Web.Script.Serialization.JavaScriptSerializer serializer =
        new System.Web.Script.Serialization.JavaScriptSerializer();
        List<Dictionary<string, object>> rows =
           new List<Dictionary<string, object>>();
        Dictionary<string, object> row = null;

        foreach (DataRow dr in dt.Rows)
        {
            row = new Dictionary<string, object>();
            foreach (DataColumn col in dt.Columns)
            {
                row.Add(col.ColumnName, dr[col]);
            }
            rows.Add(row);
        }
        return serializer.Serialize(rows);
       
    }


    [WebMethod(EnableSession = true)]
    public static string get_latest_video()
    {
        string constring = ConfigurationManager.AppSettings["DbConnection"].ToString();
        SqlConnection con = new SqlConnection(constring);
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select top 1 filename from photos where category='video' order by Id desc";
        cmd.Connection = con;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);

        DataTable dt = ds.Tables[0];
        return dt.Rows[0][0].ToString();

    }
}