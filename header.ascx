﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="header.ascx.cs" Inherits="header" %>


  <header>
            <div id="header2" class="header4-area">
                <div class="header-top-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <div class="header-top-left">
                                    <div class="logo-area">
                                        <a href="index.html">
                                            <img class="img-responsive" src="img/logo-primary.png" alt="logo"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="header-top-right">
                                    <ul>
                                        <!--<li><i class="fa fa-phone" aria-hidden="true"></i><a href="Tel:+1234567890"> + 123 456 78910 </a></li>
                                        <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#">info@academics.com</a></li>-->
                                        <li>
                                            <a class="login-btn-area" href="index.html" id="">Logout</a>
                                           
                                        </li>
                                        <li><a href="dashboard.aspx" class="apply-now-btn2">Home</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-menu-area bg-primary" id="sticker">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <nav id="desktop-nav">
                                    <ul>
                                        <li style="display:none" class="active"><a href="index.html">Home</a></li>
                                        <li style="display:none"><a href="#">About us</a>
                                            <ul>
                                                <li><a href="the-institution.html">The Institution</a></li>
                                                <li><a href="infrastructure.html">Infrastructure</a></li>
                                                <li><a href="vision-mission.html">Vision & Mission</a></li>
                                                <li><a href="Doc/News_Letter_2018_2019.pdf" target="new">News Letter</a></li>
                                                <li><a href="awards.html">Awards</a></li>
                                                <li><a href="events/annualreport.pdf" target="new">Annual Report</a></li>

                                            </ul>
                                        </li>
                                        <li style="display:none"><a  href="#">Administration</a>
                                            <ul>
                                                <li><a href="the-founder.html">The Founder</a></li>
                                                <li><a href="management.html">Management</a></li>
                                                <li><a href="principal.html">Principal</a></li>
                                                <li><a href="viceprincipal.html">Vice Principal</a></li>
                                            </ul>
                                        </li>
                                        <li style="display:none"><a href="activities.html">Activities</a></li>
                                        <li style="display:none"><a href="#">Academics</a>
                                            <ul>
                                                <li><a href="admission.html">Admission</a></li>
                                                <li><a href="students-profile.html">Students Profile</a></li>
                                                <li><a href="rules.html">Rules</a></li>
                                                <li><a href="events/results.pdf" target="new">Results</a></li>
                                            </ul>
                                        </li>

                                        <li style="display:none" ><a href="gallery.aspx">Gallery</a></li>
                                        <li style="display:none"><a href="contactus.html">Contact us</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu Area Start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                        <li class="active"><a href="index.html">Home</a></li>
                                        <li><a href="#">About us</a>
                                            <ul>
                                                <li><a href="the-institution.html">The Institution</a></li>
                                                <li><a href="infrastructure.html">Infrastructure</a></li>
                                                <li><a href="vision-mission.html">Vision & Mission</a></li>
                                                <li><a href="Doc/News_Letter_2018_2019.pdf" target="new">News Letter</a></li>
                                                <li><a href="awards.html">Awards</a></li>
                                                <li><a href="events/annualreport.pdf" target="new">Annual Report</a></li>

                                            </ul>
                                        </li>
                                        <li><a href="#">Administration</a>
                                            <ul>
                                                <li><a href="the-founder.html">The Founder</a></li>
                                                <li><a href="management.html">Management</a></li>
                                                <li><a href="principal.html">Principal</a></li>
                                                <li><a href="viceprincipal.html">Vice Principal</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="activities.html">Activities</a></li>
                                        <li><a href="#">Academics</a>
                                            <ul>
                                                <li><a href="admission.html">Admission</a></li>
                                                <li><a href="staffprofile.html" >Staffs Profile</a></li>
                                                <li><a href="students-profile.html">Students Profile</a></li>
                                                <li><a href="rules.html">Rules</a></li>
                                                <li><a href="events/results.pdf" target="new">Results</a></li>
                                            </ul>
                                        </li>

                                        <li><a href="gallery.aspx">Gallery</a></li>
                                        <li><a href="contactus.html">Contact us</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu Area End -->
        </header>