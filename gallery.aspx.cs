﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class gallery : System.Web.UI.Page
{
    SqlConnection con;
    SqlCommand cmd;
    string constring = ConfigurationManager.AppSettings["DbConnection"].ToString();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindImages("LIBRARY");
        }
    }
    public void BindImages(string category)
    {
        try
        {
            DataSet ds = new DataSet();
            using (con = new SqlConnection(constring))
            {
                cmd = new SqlCommand();
                cmd.CommandText = "select * from Photos where category='" + category + "'";
                cmd.Connection = con;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(ds);
                con.Close();
            }
            // ds_image.DataSource = ds;
            //  ds_image.DataBind();
            string html = "";
            for(int i=0;i<ds.Tables[0].Rows.Count;i++)
             html+= "<div class='col-lg-3 col-md-3 col-sm-4 col-xs-12 library' style='position: absolute; left: 0px; top: 0px; '><div class='gallery-box'><img height='200px' width='200px' src = 'gallery/" + ds.Tables[0].Rows[i][2].ToString()+"/"+ ds.Tables[0].Rows[i][1].ToString() + "' class='img-responsive' alt='gallery'>  <div class='gallery-content'>  <a href = 'gallery/" + ds.Tables[0].Rows[i][2].ToString() + "/" + ds.Tables[0].Rows[i][1].ToString() + "' class='zoom'><i class='fa fa-link' aria-hidden='true'></i></a> </div></div></div>";
            div_gallery.InnerHtml = html;
        }
        catch (Exception ex)
        {
        }

    }
    protected void ds_image_DataBinding(object sender, EventArgs e)
    {

    }
    protected void ds_image_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView dr = (DataRowView)e.Item.DataItem;
                Image ImageData = (Image)e.Item.FindControl("ImageData");
                if (dr.Row[0].ToString() != "NA")
                {
                    ImageData.Visible = true;
                    ImageData.ImageUrl = "/gallery/LIBRARY/" + dr.Row["filename"].ToString();
                }
               


            }
        }
        catch (Exception)
        { }
    }



    protected void lb_Library_Click(object sender, EventArgs e)
    {
       
        lb_Auditorium.CssClass = "test";
        lb_Library.CssClass = "current";
        lb_Classroom.CssClass = "test";
        lb_Campus.CssClass = "test";
        lb_Activities.CssClass = "test";
        BindImages("Library");
    }
    protected void lb_Classroom_Click(object sender, EventArgs e)
    {
      
        lb_Auditorium.CssClass = "test";
        lb_Library.CssClass = "test";
        lb_Classroom.CssClass = "current";
        lb_Campus.CssClass = "test";
        lb_Activities.CssClass = "test";
        BindImages("ClassRoom");
    }
    protected void lb_Auditorium_Click(object sender, EventArgs e)
    {
       
        lb_Auditorium.CssClass = "current";
        lb_Library.CssClass = "test";
        lb_Classroom.CssClass = "test";
        lb_Campus.CssClass = "test";
        lb_Activities.CssClass = "test";
        BindImages("Auditoriam");
    }
    protected void lb_Campus_Click(object sender, EventArgs e)
    {
       
        lb_Auditorium.CssClass = "test";
        lb_Library.CssClass = "test";
        lb_Classroom.CssClass = "test";
        lb_Campus.CssClass = "current";
        lb_Activities.CssClass = "test";
        BindImages("Campus");
    }
    protected void lb_Activities_Click(object sender, EventArgs e)
    {
        
        lb_Auditorium.CssClass = "test";
        lb_Library.CssClass = "test";
        lb_Classroom.CssClass = "test";
        lb_Campus.CssClass = "test";
        lb_Activities.CssClass = "current";
        BindImages("Activities");
    }
}