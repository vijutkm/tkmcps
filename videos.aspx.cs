﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class videos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        BindGrid();
    }

    private string GetImageNumber()
    {
        string dirPat = @"~/videos";

        var jpgs = System.IO.Directory.GetFiles(Server.MapPath(dirPat), "*g*.mp4").Select(f => System.IO.Path.GetFileNameWithoutExtension(f)).ToList().Select(x => x.Replace("g", string.Empty));

        var jpgs1 = jpgs.Where(x => !x.Contains("deleted_")).OrderBy(o => int.Parse(o.ToString())).ToList().LastOrDefault();

        return jpgs1;
    }

    public void BindGrid()
    {
        string dirPath = Server.MapPath("~/videos/");

        List<string> dirs = new List<string>(Directory.EnumerateDirectories(dirPath));
        List<ListItem> Imgs = new List<ListItem>();


        string dirPat = @"~/videos";

        var jpgs = System.IO.Directory.GetFiles(Server.MapPath(dirPat), "*g*.mp4").Select(f => System.IO.Path.GetFileNameWithoutExtension(f)).ToList().Select(x => x.Replace("g", string.Empty));

        var jpgs2 = jpgs.Where(x => !x.Contains("deleted_"));


        var jpgs1 = jpgs2.OrderBy(o => int.Parse(o.ToString())).ToList();

        string[] ImagePaths2 = jpgs.Select(f => f + ".mp4")
                       .ToArray();



        foreach (string imgPath in jpgs1)
        {
            string ImgName = Path.GetFileName(imgPath);
            Imgs.Add(new ListItem(ImgName, "videos/g" + ImgName + ".jpg"));
        }


        dlImages.DataSource = Imgs;

        dlImages.DataBind();

    }


    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string item = e.Row.Cells[0].Text;
            foreach (Button button in e.Row.Cells[2].Controls.OfType<Button>())
            {
                if (button.CommandName == "Delete")
                {
                    button.Attributes["onclick"] = "if(!confirm('Do you want to delete " + item + "?')){ return false; };";
                }
            }
        }
    }

    protected void dlImages_DeleteCommand(object source, DataListCommandEventArgs e)
    {

        string filename = dlImages.DataKeys[e.Item.ItemIndex].ToString();

        var filePath = Server.MapPath("~/" + filename);
        filename = filename.Split('/')[1].ToString();
        Random n = new Random();
        filename = "deleted_" + filename + (n.Next().ToString());
        var filePathDeleted = Server.MapPath("~/videos/" + filename);
        if (File.Exists(filePath))
        {

            System.IO.File.Move(filePath, filePathDeleted);
            File.Delete(filePath);

        }
        BindGrid();


    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
        {
            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            string imageNumber = GetImageNumber();
            Int32 ImageNo = Convert.ToInt32(imageNumber) + 1;
            string SaveLocation = Server.MapPath("videos") + "\\g" + ImageNo.ToString() + ".mp4";
            try
            {
                FileUpload1.PostedFile.SaveAs(SaveLocation);
                Response.Write("The file has been uploaded.");
                BindGrid();
            }
            catch (Exception ex)
            {
                Response.Write("Error Occured while uploading video. ");
            }
        }
        else
        {
            Response.Write("Please select a file to upload.");
        }
    }
}